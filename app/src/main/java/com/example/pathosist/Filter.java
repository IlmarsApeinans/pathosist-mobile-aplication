package com.example.pathosist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class Filter extends AppCompatActivity {
    //variable where trimmed image will be saved
    Bitmap croppedBitmap, originalBitmap;
    private String filePath = null;
    private ImageView imgPreview;
    ImageView croppedImageView, originalImageView;
    Uri croppedUri, origianlUri;
    TextView geolocTextView1, geolocTextView2, geolocTextView3, geolocTextView4, geolocTextView5;
    double wayLatitude, wayLongitude, defaultValue =  0.0;
    TextView tempCroppedText, tempOriginalText;
    String address, city, country;

    //variables for image upload
    private Button upload;
    long totalSize = 0;


    // LogCat tag
    private static final String TAG = MainActivity.class.getSimpleName();

    private ProgressBar progressBar;
    private TextView txtPercentage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);


// this part of code is responsive for getting cropped image from MainActivity
//        croppedImageView = (ImageView) findViewById(R.id.croppedImageView);
//        tempCroppedText = (TextView) findViewById(R.id.tempCroppedText);
//        croppedUri = (Uri)getIntent().getParcelableExtra("resID_uri");
//        tempCroppedText.setText(croppedUri.toString());

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        //this part of code is responsive for getting original image from MainActivity
        originalImageView = (ImageView) findViewById(R.id.originalImageView);
        tempOriginalText = (TextView) findViewById(R.id.tempOriginalText);

        //data from MainActivity
        origianlUri = (Uri)getIntent().getParcelableExtra("originalImage");
        wayLatitude = getIntent().getDoubleExtra("latitude", defaultValue);
        wayLongitude = getIntent().getDoubleExtra("longitude", defaultValue);
        address = getIntent().getStringExtra("address");
        city = getIntent().getStringExtra("city");
        country = getIntent().getStringExtra("country");

        filePath = origianlUri.toString();
        if (filePath != null) {
            // Displaying the image or video on the screen
            Toast.makeText(this, "okkkkkk", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
        }
        //textview initialization for testing reasons only to show what data got send from MainActivity
        //If removing/commenting, do not forget to remove/comment in activity_filter.xml
        tempOriginalText.setText(origianlUri.toString());
        geolocTextView1 = findViewById(R.id.text_view_location_1);
        geolocTextView2 = findViewById(R.id.text_view_location_2);
        geolocTextView3 = findViewById(R.id.text_view_location_3);
        geolocTextView4 = findViewById(R.id.text_view_location_4);
        geolocTextView5 = findViewById(R.id.text_view_location_5);

        //assigne unload image button variable
        upload = findViewById(R.id.upload);
        //this part of code is responsive for showing cropped image to user in Filter activity
//        try {
//            croppedBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), croppedUri);
////            bitmap = rotateImageIfRequired(this, bitmap , uri);
//            croppedImageView.setImageBitmap(croppedBitmap);
//            // not sure why this happens, but without this the image appears on its side
//            croppedImageView.setRotation(croppedImageView.getRotation() + 90);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        //this port of code is responsive for showing original image tu user in Filter activity
        try {
            originalBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), origianlUri);
//          bitmap = rotateImageIfRequired(this, bitmap , uri);
            originalImageView.setImageBitmap(originalBitmap);
            // not sure why this happens, but without this the image appears on its side
            originalImageView.setRotation(originalImageView.getRotation() + 90);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //setting data that was got from MainActivity to be shown in Filter activity,
        //if initialization of those textview has been commented, then this part has to be commented as well
        geolocTextView1.setText(String.valueOf(wayLatitude));
        geolocTextView2.setText(String.valueOf(wayLongitude));
        geolocTextView3.setText(address);
        geolocTextView4.setText(city);
        geolocTextView5.setText(country);

        upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // uploading the file to server
                new UploadFileToServer().execute();
            }
        });
    }


    /**
     * Uploading the file to server
     * */
    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress(progress[0]);

            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.FILE_UPLOAD_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });
//                File sourceFile = new File(origianlUri.getPath());
                File sourceFile = new File(getPath(origianlUri));
//                File sourceFile = new File(String.valueOf(origianlUri));
                // Adding file data to http body
                entity.addPart("image", new FileBody(sourceFile));
                // Extra parameters if you want to pass to server
                entity.addPart("latitude", new StringBody(String.valueOf(wayLatitude)));
                entity.addPart("longitude", new StringBody(String.valueOf(wayLongitude)));
                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);

            // showing the server response in an alert dialog
            showAlert(result);

            super.onPostExecute(result);
        }

    }




    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index =             cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }


    private void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setTitle("Response from Servers")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }



}