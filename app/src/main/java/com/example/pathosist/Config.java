package com.example.pathosist;

public class Config {

    // File upload url (replace the ip with your server address)
    public static final String FILE_UPLOAD_URL = "http://www.digitalmuseumtest.lv/AndroidFileUpload/fileUpload.php";

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";
}